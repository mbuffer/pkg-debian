mbuffer (20110119-3) UNRELEASED; urgency=medium

  * Fix the watch file: support both absolute and relative links.
  * Remove the obsolete DM-Upload-Allowed source control field.
  * Bump the debhelper compatibility version to 9:
    - get the build hardening flags directly from debhelper
    - drop the dpkg-dev versioned dependency (for dpkg-buildflags),
      long satisfied even in oldstable
  * Drop the version in the autotools-dev build dependency.
  * Drop the source compression options; dpkg-dev's defaults are good enough.
  * Switch the copyright file to the final version of the 1.0 specification
    and bump the year of my debian/* copyright notice.
  * Bump Standards-Version to 3.9.6 with no changes.
  * Mark the binary package as Multi-Arch: foreign.
  * Add a debian/upstream/metadata file.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 21 Oct 2014 15:32:10 +0300

mbuffer (20110119-2) unstable; urgency=low

  * Fix the watchfile: the upstream download page's URLs are fully
    qualified now.
  * Fix the FTBFS on GNU/Hurd by trying fpathconf() before PIPE_BUF and
    checking for several more constants if that fails.
  * Update the copyright file to the latest DEP 5 candidate format and
    add the GPL-3+ license in the header, too, just in case.
  * Upload to unstable.

 -- Peter Pentchev <roam@ringlet.net>  Thu, 10 Mar 2011 14:54:13 +0200

mbuffer (20110119-1) experimental; urgency=low

  * Switch to Git and point the Vcs-* fields to Gitorious.
  * Bump Standards-Version to 3.9.1 with no changes.
  * Switch to bzip2 compression for the Debian tarball.
  * Bump the debhelper compatibility level to 8 and reorder
    the dh(1) arguments.
  * New upstream release (10th anniversary!)
  * Convert the copyright file to DEP 5 rev. 153 (candidate) and
    bump the year on my copyright notice.

 -- Peter Pentchev <roam@ringlet.net>  Thu, 20 Jan 2011 16:52:02 +0200

mbuffer (20100526-2) unstable; urgency=low

  * Shorten the Vcs-Browser URL.
  * Bump Standards-Version to 3.9.0 with no changes.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 29 Jun 2010 01:03:28 +0300

mbuffer (20100526-1) unstable; urgency=low

  * New upstream release.
  * Use dpkg-buildflags from dpkg-dev >= 1.15.7~ to obtain CFLAGS and
    CPPFLAGS and to add -Wl,-z,defs to LDFLAGS; no longer rely on
    dpkg-buildpackage to export the flags variables automatically.

 -- Peter Pentchev <roam@ringlet.net>  Wed, 02 Jun 2010 13:13:59 +0300

mbuffer (20100327-1) unstable; urgency=low

  [ Peter Pentchev ]
  * New upstream version.
    - remove the 07-manpage patch, integrated upstream
    - update the copyright years for mbuffer.c
  * Use the debhelper plugin provided by autotools-dev-20100122.1 to
    refresh the config.sub and config.guess files.
  * Bring the copyright file up to DEP 5 rev. 135:
    - move the author's name and e-mail to the top-level Maintainer field
    - also remove the Author field from the debian/* section
  * Bump the year in my debian/* copyright notice.
  * Just set LDFLAGS instead of overriding debhelper's auto_configure.

  [ Hector Oron ]
  * Set DM-Upload-Allowed for Peter Pentchev.

 --  <zumbi@debian.org>  Wed, 07 Apr 2010 13:56:41 +0000

mbuffer (20091227-1) unstable; urgency=low

  * New upstream version:
    - remove the 05-kfreebsd and 06-signal patches, integrated upstream
    - reintroduce the 02-test-cleanup patch, since the upstream Makefile
      needs to be portable and may not afford to use ?=
    - remove the explicit -lrt, upstream links against it now
  * Bump Standards-Version to 3.8.4 with no changes.
  * Add the 07-manpage patch to fix a couple of typos and grammar nits
    in the manual page; forwarded upstream, the author accepted it for
    the next upstream release.

 -- Peter Pentchev <roam@ringlet.net>  Thu, 25 Feb 2010 16:43:44 +0200

mbuffer (20091213-1) unstable; urgency=low

  * Fix FTBFS on GNU/kFreeBSD.  Closes: #560750
  * Honor upstream's preference for libmhash and drop the GnuTLS patch.
    Pointed out by: Mats Erik Andersson <mats.andersson@gisladisker.se>
  * New upstream version:
    - integrate the test suite and the hstrerror -lresolv patches
    - honor the user's CFLAGS, so no need for the CFLAGS patch
    - link with -lrt for clock_gettime(3) and friends
    - portability fixes: read a monotonic clock, make IPv6 really optional,
      check the input device's block size
  * Fix a segfault when terminating in quiet mode.  Closes: #562112.

 -- Peter Pentchev <roam@ringlet.net>  Thu, 24 Dec 2009 17:51:50 +0200

mbuffer (20091122-1) unstable; urgency=low

  * Initial release (Closes: #534216)

 -- Peter Pentchev <roam@ringlet.net>  Thu, 10 Dec 2009 23:52:14 +0200
